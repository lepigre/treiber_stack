all: Treiber_stack.vo Treiber_stack.html

%.vo %.glob: %.v
	@echo "[COQC] $<"
	@coqc $<

%.html: %.v %.glob
	@echo "[CDOC] $<"
	@coqdoc --utf8 --html --no-index $<

clean:
	@rm -f *.vo *.glob .*.aux *.html coqdoc.css

distclean: clean
	@rm -f *~
